use rand::prelude::*;
use std::cmp::{min, Ordering};

pub type AbilityScore = u8;
pub type AbilityScores = [AbilityScore; 6];
pub type AbilityScoreHistogram = [u32; 21];

pub fn generate_ability_score_histogram(results: &Vec<AbilityScores>) -> AbilityScoreHistogram
{
    let mut histogram: AbilityScoreHistogram = [0; 21];

    for ability_score_set in results.iter()
    {
        for ability_score in ability_score_set.iter()
        {
            histogram[(*ability_score) as usize] += 1;
        }
    }

    return histogram;
}

pub fn histogram_to_string(histogram: &AbilityScoreHistogram) -> String
{
    let mut to_string: String = String::new();
    let mut total_count: u32 = 0;
    for i in 3..=18
    {
        to_string += format!("{}\t{}\n", i, histogram[i]).as_str();
        total_count += histogram[i];
    }

    return format!("Total entries: {}, entries:\n{}", total_count, to_string);
}

pub fn get_median_ability_scores(results: &Vec<AbilityScores>) -> AbilityScores
{
    let mut sorted_results: Vec<AbilityScores> = results.clone();
    for ability_scores in sorted_results.iter_mut()
    {
        ability_scores.sort_unstable();
    }

    sorted_results.sort_unstable_by(|lhs, rhs| {
        let lhs_sum: u8 = lhs.iter().sum();
        let rhs_sum: u8 = rhs.iter().sum();
        let comparison = u8::cmp(&lhs_sum, &rhs_sum);
        if comparison != Ordering::Equal
        {
            return comparison;
        }

        for i in (0..6).rev()
        {
            let comparison = u8::cmp(&lhs[i], &rhs[i]);
            if comparison != Ordering::Equal
            {
                return comparison;
            }
        }
        return Ordering::Equal;
    });

    if sorted_results.len() & 2 == 0
    {
        println!("sorted_results.len(): {}, sorted_results.len() & 2: {}", sorted_results.len(), sorted_results.len() & 2);
        let middle_index = sorted_results.len() / 2;
        let middle_index2 = middle_index - 1;

        let mid_abil_scores_1 = sorted_results[middle_index];
        let mid_abil_scores_2 = sorted_results[middle_index2];

        return [
            f64::round((mid_abil_scores_1[0] as f64 + mid_abil_scores_2[0] as f64) / 2.0f64) as u8,
            f64::round((mid_abil_scores_1[1] as f64 + mid_abil_scores_2[1] as f64) / 2.0f64) as u8,
            f64::round((mid_abil_scores_1[2] as f64 + mid_abil_scores_2[2] as f64) / 2.0f64) as u8,
            f64::round((mid_abil_scores_1[3] as f64 + mid_abil_scores_2[3] as f64) / 2.0f64) as u8,
            f64::round((mid_abil_scores_1[4] as f64 + mid_abil_scores_2[4] as f64) / 2.0f64) as u8,
            f64::round((mid_abil_scores_1[5] as f64 + mid_abil_scores_2[5] as f64) / 2.0f64) as u8
        ];
    }
    return sorted_results[sorted_results.len() / 2];
}

pub fn four_d6_drop_lowest_til_2_above_15_n_times(num_trials: u32)
{
    let mut results: Vec<AbilityScoreGenerationStats> = Vec::with_capacity(num_trials as usize);

    for trial in 0..num_trials {
        results.push(gen_ability_scores_til_2_at_least_15());
    }

    let total_gen_count: u64 = results.iter().map(|s| (s.gen_count as u64)).sum();
    let average_gen_count: f64 = (total_gen_count as f64) / (num_trials as f64);

    let total_ability_score_sum: u64 = results.iter().map(|s| (s.ability_scores.iter().sum::<u8>()) as u64).sum();
    let average_ability_scores_sum: f64 = (total_ability_score_sum as f64) / (num_trials as f64);
    println!("ability_score.rs::four_d6_drop_lowest_til_above_15 - total_gen_count: {}, average_gen_count: {}, average_ability_scores_sum: {}",
             total_gen_count,
             average_gen_count,
             average_ability_scores_sum);

    let ability_scores: Vec<AbilityScores> = results.iter().map(|s| s.ability_scores).collect();
    let histogram: AbilityScoreHistogram = generate_ability_score_histogram(&ability_scores);
    println!("ability_score.rs::four_d6_drop_lowest_til_above_15 - histogram:\n{}", histogram_to_string(&histogram));
    println!("ability_score.rs::four_d6_drop_lowest_til_above_15 - median_ability_scores: {:?}", get_median_ability_scores(&ability_scores));
}

pub fn four_d6_drop_lowest_n_times(num_trials: u32)
{
    let mut results: Vec<AbilityScores> = Vec::with_capacity(num_trials as usize);

    for trial in 0..num_trials {
        results.push(generate_ability_scores());
    }

    let total_ability_score_sum: u64 = results.iter().map(|s| (s.iter().sum::<u8>()) as u64).sum();
    let average_ability_scores_sum: f64 = (total_ability_score_sum as f64) / (num_trials as f64);
    println!("ability_score.rs::four_d6_drop_lowest_n_times - average_ability_scores_sum: {}", average_ability_scores_sum);

    let histogram: AbilityScoreHistogram = generate_ability_score_histogram(&results);
    println!("ability_score.rs::four_d6_drop_lowest_n_times - histogram:\n{}", histogram_to_string(&histogram));
    println!("ability_score.rs::four_d6_drop_lowest_n_times - median_ability_scores: {:?}", get_median_ability_scores(&results));
}

pub fn four_d6_drop_lowest_replace_2_lowest_n_times(num_trials: u32)
{
    let mut results: Vec<AbilityScores> = Vec::with_capacity(num_trials as usize);

    for trial in 0..num_trials {
        results.push(four_d6_drop_lowest_replace_2_lowest());
    }

    let total_ability_score_sum: u64 = results.iter().map(|s| (s.iter().sum::<u8>()) as u64).sum();
    let average_ability_scores_sum: f64 = (total_ability_score_sum as f64) / (num_trials as f64);
    println!("ability_score.rs::four_d6_drop_lowest_replace_2_lowest_n_times - average_ability_scores_sum: {}", average_ability_scores_sum);

    let histogram: AbilityScoreHistogram = generate_ability_score_histogram(&results);
    println!("ability_score.rs::four_d6_drop_lowest_replace_2_lowest_n_times - histogram:\n{}", histogram_to_string(&histogram));
    println!("ability_score.rs::four_d6_drop_lowest_replace_2_lowest_n_times - median_ability_scores: {:?}", get_median_ability_scores(&results));
}

pub fn four_d6_drop_lowest_replace_2_lowest() -> AbilityScores
{
    let mut ability_scores: AbilityScores =
        [
            four_d6_drop_lowest(),
            four_d6_drop_lowest(),
            four_d6_drop_lowest(),
            four_d6_drop_lowest(),
            four_d6_drop_lowest(),
            four_d6_drop_lowest()
        ];

    let fifteens_count = ability_scores.iter().filter(|&x| *x >= 15).count();
    if fifteens_count >= 2
    {
        return ability_scores;
    }

    ability_scores.sort_unstable();
    let mut rng = rand::thread_rng();
    if fifteens_count == 0
    {
        ability_scores[1] = rng.gen_range(15, 19);
    }

    if fifteens_count == 1
    {
        ability_scores[1] = rng.gen_range(15, 19);
    }

    return ability_scores;
}

pub fn four_d6_drop_lowest() -> AbilityScore
{
    let mut rng = rand::thread_rng();
    let first_d6: AbilityScore = rng.gen_range(1, 7);
    let second_d6: AbilityScore = rng.gen_range(1, 7);
    let third_d6: AbilityScore = rng.gen_range(1, 7);
    let fourth_d6: AbilityScore = rng.gen_range(1, 7);
    let min_val: AbilityScore = min(min(first_d6, second_d6), min(third_d6, fourth_d6));
    return first_d6 + second_d6 + third_d6 + fourth_d6 - min_val;
}

pub fn generate_ability_scores() -> AbilityScores
{
    let ability_scores: AbilityScores =
        [
            four_d6_drop_lowest(),
            four_d6_drop_lowest(),
            four_d6_drop_lowest(),
            four_d6_drop_lowest(),
            four_d6_drop_lowest(),
            four_d6_drop_lowest()
        ];
    return ability_scores;
}

#[derive(Debug)]
pub struct AbilityScoreGenerationStats {
    pub ability_scores: AbilityScores,
    pub gen_count: u16,
}

pub fn gen_ability_scores_til_2_at_least_15() -> AbilityScoreGenerationStats
{
    let mut gen_count = 0;
    loop {
        let ability_scores: AbilityScores = generate_ability_scores();
        gen_count += 1;
        let count = ability_scores.iter().filter(|&x| *x >= 15).count();
        if count >= 2
        {
            return AbilityScoreGenerationStats {
                ability_scores,
                gen_count,
            };
        }
    }
}

pub fn generate_ability_scores_with_d20() -> AbilityScores
{
    let mut rng = rand::thread_rng();
    let ability_scores: AbilityScores =
        [
            rng.gen_range(1, 21),
            rng.gen_range(1, 21),
            rng.gen_range(1, 21),
            rng.gen_range(1, 21),
            rng.gen_range(1, 21),
            rng.gen_range(1, 21),
        ];
    return ability_scores;
}

pub fn generate_ability_scores_with_d20_n_times(num_trials: u32)
{
    let mut results: Vec<AbilityScores> = Vec::with_capacity(num_trials as usize);

    for trial in 0..num_trials {
        results.push(generate_ability_scores_with_d20());
    }

    let total_ability_score_sum: u64 = results.iter().map(|s| (s.iter().sum::<u8>()) as u64).sum();
    let average_ability_scores_sum: f64 = (total_ability_score_sum as f64) / (num_trials as f64);
    println!("ability_score.rs::generate_ability_scores_with_d20_n_times - average_ability_scores_sum: {}", average_ability_scores_sum);

    let histogram: AbilityScoreHistogram = generate_ability_score_histogram(&results);
    println!("ability_score.rs::generate_ability_scores_with_d20_n_times - histogram:\n{}", histogram_to_string(&histogram));
    println!("ability_score.rs::generate_ability_scores_with_d20_n_times - median_ability_scores: {:?}", get_median_ability_scores(&results));
}
