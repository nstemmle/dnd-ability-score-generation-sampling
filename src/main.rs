mod ability_score;

fn main() {
    let num_trials = 100000;
    ability_score::four_d6_drop_lowest_til_2_above_15_n_times(num_trials);
    println!();
    ability_score::four_d6_drop_lowest_n_times(num_trials);
    println!();
    ability_score::four_d6_drop_lowest_replace_2_lowest_n_times(num_trials);
}
